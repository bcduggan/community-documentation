---
title: Initial Reply - For Arabic Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Arabic speakers"
sidebar: mydoc_sidebar
permalink: 274-Initial_Reply_For_Arabic_Speakers.html
folder: mydoc
conf: Public
lang: ar
---


# Initial Reply
## First response, Email to Client for Arabic speakers

### Body


مرحبا $ClientName,،

شكرا لمراسلتك لفريق مساعدو الأمن الرقمي بأكساس ناو
,(https://www.accessnow.org/%D9%85%D8%B3%D8%A7%D8%B9%D8%AF%D9%88-%D8%A7%D9%84%D8%A3%D9%85%D8%A7%D9%86-%D8%A7%D9%84%D8%B1%D9%82%D9%85%D9%8A/)
 أنا إسمي
$IHName,
و أنا هنا لتقديم المساعدة اللازمة لك.

يرجى العلم بأن فريق مساعدو الأمن الرقمي مدار بقبل مجموعة من المتخصيصين في الأمن الرقمي يشتغلون ضمن مناطق ذات فارق في التوقيت. لذلك أعضاء أخرون من الفريق قد يتجاوبو مع مراسلتك إعتمادا على التوقيت و اليوم الذي راسلتنا فيه. سنتابع أنا أو زميل من زملائي حول مراسلتك في وقت قصير حتى نستطيع أن نطلب منك تفاصيل إضافية أو سياق تكميلي إذا احتجنا لكي نستطيع العمل على طلبك.

يرجى مواصلة الأستحفاظ على
“[accessnow #ID]"
في خانة  الموضوع  لكل المحاورات المستقبلية حول هاته المحادثة.  هذا رقم المرجع  المولد من طرف نظام ترتيب الرسائل الخاص بنا حتى نتمكن من تنظيم و متابعة المهام المتعلقه بطلبك.

أطيب التحيات،
$IHName


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
