---
title: Response to 1st-Time/Non-Vetted Helpline Client for Spanish Speakers
keywords: case handling policy, initial reply, non-vetted
last_updated: May 26, 2020
tags: [helpline_procedures_templates, templates]
summary: "First reply - questions and introduction to vetting in spanish"
sidebar: mydoc_sidebar
permalink: ###-response_first-time_client_spanish_speakers.html
folder: mydoc
conf: Public
ref: response_first-time_client_spanish_speakers
lang: es
---


# Response to 1st-Time/Non-Vetted Helpline Client for Spanish Speakers

## First reply - questions and introduction to vetting

### Body

Hola,

Muchas gracias por contactar a la Línea de Ayuda de Access Now. De nuevo, soy [$IH Name]. Apreciamos su confianza en la Línea de Ayuda y el servicio que ofrecemos. La Línea de Ayuda en Seguridad Digital de Access Now es un recurso gratuito para la sociedad civil alrededor del mundo. Ofrecemos asistencia técnica directa en tiempo real y asesoramiento en temas de seguridad digital para grupos de la sociedad civil, activistas, medios de comunicación, periodistas, blogueros y defensores de derechos humanos.

Estamos felices de ayudar, primero nos gustaría saber alguna información extra.

1. ¿Cómo se enteró de la Línea de Ayuda de Access Now? ¿Alguna persona le referenció con la Línea de Ayuda? Si fue así, ¿podría decirnos quien realizó la referencia?

2. Por favor cuentenos acerca de su trabajo. ¿Pertenece a alguna organización o grupo?

3. Si hay disponible, puede compartirnos enlaces de su sitio web o el de su organización en el que se muestre el trabajo que realiza en la sociedad civil y el movimientos de derechos humanos?

4. Le agradecemos si nos pudiera indicar el nombre y el pronombre con el cual prefiere que nos dirigamos a usted.

* * *

Queremos informarle que, como parte de nuestras medidas de seguridad, verificamos la identidad de nuevos potenciales clientes de la Línea de Ayuda. Esto significa que contactamos a personas de confianza y compartimos información como su nombre, correo electrónico y organización a la que pertenece. Tenga la certeza que cualquier otra información relacionada a su caso permanecerá estrictamente confidencial y disponible únicamente para el equipo de la Línea de Ayuda.

Como parte de este proceso y para agilizar el mismo, nos sería de ayuda si nos puede brindar la información de contacto de al menos dos personas dentro de la sociedad civil que puedan verificar su identidad, y llenando esta encuesta sobre usted y su el trabajo que realiza:

https://form.accessnow.org/index.php/652353/Y?ticket=[TICKET_ID]

Si usted no desea que continuemos con este proceso, por favor déjenoslo saber en las próximas 48 horas y nosotros intentaremos encontrar una forma alternativa para realizar la verificación.

* * *

Entendemos que alguna información que podemos intercambiar podría ser sensible y confidencial, por lo que podemos establecer un canal de comunicación seguro. Si ya utiliza cifrado PGP, por favor envíenos su llave pública. En cualquier otro caso, díganos si utiliza algún otro canal de comunicación seguro, como Signal Messenger, Wire.com, XMPP/OTR, etc.

* * *

Finalmente, tenga en cuenta que los servicios de la Línea de Ayuda de Access Now no están dirigidos para personas menores de 18 años de edad. Si este es su caso, podemos dirigirle al lugar en donde le pueden ofrecer la asistencia que necesita.

Saludos cordiales,

$IH Name


* * *


### Related Articles
Article #154: FAQ - Initial Reply
Article #268: Initial Reply for Harassment Cases
Article #256: Response to 1st-Time/Non-Vetted Helpline Client
