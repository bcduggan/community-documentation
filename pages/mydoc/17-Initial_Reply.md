---
title: Initial Reply in English
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client"
sidebar: mydoc_sidebar
permalink: 17-Initial_Reply.html
folder: mydoc
conf: Public
lang: en
---


# Initial Reply in English
## First response, Email to English-speaking Client

### Body

Dear $ClientName,

Thank you for contacting the Access Now Digital Security Helpline
(https://www.accessnow.org/help), my name is $IHName and i am here to
help you.

We have received your initial request and my team is now working on it.

Please be informed that the Helpline is operated by a group of security
professionals working from different time zones. Therefore, different
team members may answer to your messages depending on the time and day
it was received. Myself, or one of my colleagues will follow-up with you
shortly to request for additional details or supplementary context
needed to process your request.

Please continue to include “[accessnow #ID]” in the subject line for all
future correspondence about this issue. This is a reference ID generated
by our ticketing system to manage and coordinate tasks for your request.

We would be grateful if you could tell us what name and pronoun/s you would like to be addressed with.

Thanks,
$IHName
