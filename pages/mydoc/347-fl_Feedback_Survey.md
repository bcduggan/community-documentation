---
title: Filipino - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "Email to be sent to Filipino-speaking clients or requestors when closing a ticket"
sidebar: mydoc_sidebar
permalink: 347-fl_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: fl
---


# Feedback Survey Template - Filipino
## Email to be sent to Filipino-speaking clients or requestors when closing a ticket

### Body

Mahal na $ClientName,

Salamat sa pakikipag-ugnay sa Digital Security Helpline na pinapatakbo ng pandaigdigang organisasyon ng karapatang pantao na Access Now - [Home - Access Now](https://accessnow.org.)

Ang mensaheng ito ay upang ipaalam sa iyo ang pagsasara ng iyong kaso na pinamagatang "{$Ticket-&gt;Subject}".

Mahalaga sa amin ang iyong katugunan. Kung nais mong magbigay ng katugunan tungkol sa iyong karanasan sa Access Now Digital Security Helpline, mangyaring punan ang sumusunod na pagsusuri:
[Access Now's Digital Security Helpline Feedback](https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id})

Ang bilang ng iyong kaso ay: {$Ticket-&gt;id}

Kung mayroon kang anumang karagdagang mga katanungan o alalahanin, mangyaring ipaalam lamang sa amin at masaya kaming tutulong.

Salamat,

$IHName
