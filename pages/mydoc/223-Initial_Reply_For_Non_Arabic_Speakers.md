---
title: Initial Reply - For Non-Arabic Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Arabic speakers"
sidebar: mydoc_sidebar
permalink: 223-Initial_Reply_For_Non_Arabic_Speakers.html
folder: mydoc
conf: Public
lang: ar
---

### Body

أهلا بك،

أنا 
[Your Name] 
عضو من فريق مساعدي الأمان الرقمي لأكساس ناو.
مساعدو-الأمان-الرقمي
https://www.accessnow.org/
 
لقد تلقينا البريد الإلكتروني الذي بعثته 
[Email Subject]
مع الأسف لن نستطيع التواصل معك بالعربية 
في صورة ما إذا تتطلب الوضعية التدخل السريع، الرجاء الإجابة و إضافة كلمة 
“URGENT”
إلى موضوع البريد الإلكتروني. 

إن كنتم قادرين على التواصل باللغة

الإنجليزية [EN]

أو الفرنسية [FR]

أو الألمانية [DE]

أو الإسبانية [ES]

أو الإيطالية [IT] 

أو البرتغالية [PT]

أو الروسية [RU]

سنكون قادرين على الإجابة على أسئلتكم مباشرة الآن 

سيكون زملاؤنا الذين يتكلمون العربية على الخط بعد سويعات 

شكراً .

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
