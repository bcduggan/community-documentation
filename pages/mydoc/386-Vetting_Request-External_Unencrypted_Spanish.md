---
title: Vetting Request - External Unencrypted for Spanish Speakers
keywords: email templates, vetting, vetting process, partner
last_updated: May 27, 2020
tags: [helpline_procedures_templates, templates]
summary: "External Vetting, Email to Contact if unencrypted in spanish"
sidebar: mydoc_sidebar
permalink: 386-Vetting_Request-External_Unencrypted_Spanish.html
folder: mydoc
conf: Public
ref: Vetting_Request-External_Unencrypted_ES
lang: es
---


#  Vetting Request - External Unencrypted for Spanish Speakers
## External Vetting, Email to Contact if unencrypted 
### Body

Hola [NAME],

Mi nombre es [IH's name] y soy parte de la Línea de Ayuda de Seguridad Digital de Acces Now (accessnow.org). Recibí su información de contacto por parte de una persona usuaria de nuestros servicios. Le contacto muy respetuosamente para solicitar su asistencia para verificar la identidad de una nueva persona usuaria de la Línea de Ayuda.

Como parte de nuestros procedimientos, requerimos que personas dentro de nuestra red de confianza nos colaboren verificando la identidad de nuevas personas usuarias. Para poder comunicarle los detalles, necesitamos entablar un canal seguro de comunicación.

Si ya contara con una llave pública PGP, le agradecemos que nos la envié, sino le podemos asistir en el proceso de configurar PGP para correos encriptados o podríamos utilizar Signal para mensajes instantáneos encriptados. Por favor indíquenos si necesita alguna ayuda.

Gracias de antemano,

$IH Name

* * *


### Related Articles
Article #5: FAQ - Vetting Request - External Unencrypted
