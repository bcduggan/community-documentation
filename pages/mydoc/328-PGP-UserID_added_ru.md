---
title: PGP - User ID added - Russian
keywords: email templates, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: February 7, 2019
tags: [secure_communications_templates, templates]
summary: "Сообщение для клиента о том, что его новый ID пользователя добавлен к его ключу PGP"
sidebar: mydoc_sidebar
permalink: 328-PGP-UserID_added_ru.html
folder: mydoc
conf: Public
ref: PGP-UserID_added
lang: ru
---


# PGP - User ID added
## Email to client to confirm that their new user ID has been added to their PGP key

### Body

Здравствуйте,

Насколько я могу судить по информации с серверов ключей, ваш новый ID пользователя [ПОЧТОВЫЙ АДРЕС] теперь связан с вашим ключом PGP.

Я закрываю этот запрос. Мне будет приятно помочь вам в будущем, если у вас возникнет такая потребность.

С уважением,

[ИМЯ СОТРУДНИКА]



* * *


### Related Articles

- [Article #19: PGP - Add UserID](19-PGP_add_userID.html)
