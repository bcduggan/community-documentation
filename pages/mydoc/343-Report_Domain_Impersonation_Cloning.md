---
title: NEW NEW NEW!!! Fake Domain - Report Impersonation/Cloning to Domain Provider
keywords: fake domain, impersonation, cloning, escalations, domain provider, take-down request, report template
last_updated: March 1, 2019
tags: [fake_domain_templates, templates] 
summary: "Email template to report cases of domain impersonation and request take-down of the violating domain"
sidebar: mydoc_sidebar
permalink: 343-Report_Domain_Impersonation_Cloning.html
folder: mydoc
conf: Confidential
lang: en
---


# Fake Domain - Template to Report Impersonation/Cloning to Domain Provider
## Email template to report cases of domain impersonation and request them take down the violating domain

### Body

TO: abuse@registrardomainname
SUBJECT: Impersonation Report

Dear {{ beneficiary name }},

My name is {{ incident handler name }} from Access Now Digital Security Helpline. We are a free-of-charge resource for civil society around the world. I'm respectfully contacting you to ask for your assistance with a digital security issue that was recently reported to our Helpline.

A website with this URL: {{ URL }}, which is hosted by {{ hosting/domain provider name }}, is impersonating this legitimate website: {{ Legitimate Website URL }} 

{{ Explanations of client's ownership/trademark registration and adversary's intent }}

{{ Specific link/description of impersonated materials }}

We are asking for your assistance in taking down the fake website:

* Fake website: {{ URL }}

* Original website: {{ URL }}

In advance, we appreciate your responsiveness, and gently request your immediate action to solve the above-mentioned situation. 

Kind regards,

{{ IH’s name }}


* * *


### Related Articles

[Article #342: Handling of Fake Domains](342-Handling_Fake_Domains.html)
