---
title: Outreach Template for Local Situations - Russian
keywords: email templates, outreach, local situations
last_updated: February 7, 2019
tags: [helpline_procedures_templates, templates]
summary: "Шаблон для сообщения клиенту, у которого могут быть проблема из-за ситуации в регионе"
sidebar: mydoc_sidebar
permalink: 333-Situations_template_ru.html
folder: mydoc
conf: Public
ref: Situations_template
lang: ru
---


# Outreach Template for Local Situations
## Template for reaching out to clients who might be affected by a local situation

### Body

Здравствуйте,

Меня зовут [ИМЯ СОТРУДНИКА]. Я работаю с инцидентами в Службе поддержки по вопросам цифровой безопасности организации Access Now. Вы обращались в нашу службу ранее. Я пишу потому, что служба поддержки получила данные о критической ситуации в вашем регионе. [ОПИСАНИЕ СИТУАЦИИ].

Полагаем, что из-за этой сложной ситуации повышен риск следующих угроз: [УГРОЗЫ ЦИФРОВОЙ БЕЗОПАСНОСТИ]. Возможно, вы захотите принять превентивные меры:

- [ПРЕВЕНТИВНАЯ МЕРА]
- [ПРЕВЕНТИВНАЯ МЕРА]

Буду рад помочь, если вам нужна поддержка с этими рекомендациями или с решением любой иной проблемы.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles

- [Article #276: Outreach to Clients for Local Situations](276-Situations.html)
