---
title: Content Removal - Standard Reporting Mechanisms
keywords: harassment, content moderation, content removal, remove accounts, takedown, standard reporting mechanisms
last_updated: February 18, 2020
tags: [harassment_templates, templates]
summary: "Template to point the client to the reporting mechanisms of the different social media platforms"
sidebar: mydoc_sidebar
permalink: 372-Content_Removal_Standard_Reporting_Mechanisms.html
folder: mydoc
conf: Public
ref: Content_Removal_Standard_Reporting_Mechanisms
lang: en
---


# Content Removal - Standard Reporting Mechanisms
## Template to point the client to the reporting mechanisms of the different social media platforms

### Body

Dear [$NAME],

My name is [$HANDLER_NAME], and I'm part of Access Now Digital Security Helpline. Thank you for reaching out.

We have reviewed your request and would like to suggest you report the issue directly to the platform. In order to do so, please follow the instructions in the following links: 

- [Facebook](https://www.facebook.com/help/181495968648557/)
- [Twitter](https://help.twitter.com/en/rules-and-policies/twitter-report-violation)
- [Instagram](https://help.instagram.com/519598734752872)
- [YouTube](https://support.google.com/youtube/answer/2802027?hl=en&amp;ref_topic=9387085)

If the situation escalates, or you believe that the content in question poses a physical risk to someone, please let us know. 

Kind regards,

[IH's Name]   



* * *


### Related Articles

- [Article #371: Content Moderation - Requests for Content Removal](371-content_removal.html)
