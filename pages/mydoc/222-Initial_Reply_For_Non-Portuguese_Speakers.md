---
title: Initial Reply - For Non-Portuguese Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Portuguese speakers"
sidebar: mydoc_sidebar
permalink: 222-Initial_Reply_For_Non-Portuguese_Speakers.html
folder: mydoc
conf: Public
lang: pt
---


# Initial Reply - For Non-Portuguese Speakers
## First response, Email to Client if you're a non-Portuguese speaker

### Body


Olá [Client Name],

Meu nome é [Your Name], eu sou parte da equipe da Linha de Ajuda en Segurança Digital na Access Now - https://accessnow.org/help

Recebemos com sucesso a sua mensagem [Email Subject]. Infelizmente eu não sou um falante fluente da sua língua nativa. Se este é um pedido urgente, responda a este e-mail adicionando a etiqueta "URGENT" à linha de assunto e iremos fornecer-lhe a atenção necessária o mais rápido possível.

Se você se sentir confortável mover a conversa para Inglês o [Languages spoken by incident handler Spanish=Espanhol, French=Francês, Arabic=Arábico], terei o maior prazer em responder às suas perguntas diretamente.. 

Nossos colegas que falam Português, vão estar no escritório em poucas horas para responder.

Cumprimentos,

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
