---
title: French - Feedback Survey Template
keywords: modèle d'email, commentaires des clients, politique de traitement des incidents, suivi 
last_updated: March 19, 2019
tags: [helpline_procedures_templates, templates]
summary: "Modèle d'un email pour demander l'avis des clients à propos leurs demandes à la fin de chaque ticket"
sidebar: mydoc_sidebar
permalink: 351-fr_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: fr
---


# French - Feedback Survey Template
## Modèle d'un email pour demander l'avis des clients à propos leurs demandes à la fin de chaque ticket

### Body


Bonjour $ClientName,

Merci de contacter l'équipe Helpline, gérée par l’organisation internationale des droits de l’homme Access Now - https://accessnow.org.

Ce message a pour but de vous informer de la clôture de votre incident, intitulé "{$Ticket-&gt;Subject}".

Votre avis est important pour nous. Si vous souhaitez nous faire contribuer avec vos commentaires sur votre expérience avec l'équipe Helpline Access Now, veuillez vous s'il vous plaît remplir le questionnaire suivant:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Le numero de votre incident est: {$Ticket-&gt;id}

Si vous avez d'autres questions ou préoccupations, veuillez nous informer et nous serons heureux de fournir notre assistance.

Merci,

$IHName
