---
title: PGP - Extend Expiration Date - Confirmation Email - Russian
keywords: email templates, PGP, key management, expiration date, Enigmail, Thunderbird, email security
last_updated: February 7, 2019
tags: [secure_communications_templates, templates]
summary: "Письмо клиенту с подтверждением, что ключ PGP успешно продлен"
sidebar: mydoc_sidebar
permalink: 322-PGP_Extend_Expiration_Confirmation_Email_ru.html
folder: mydoc
conf: Public
ref: PGP_Extend_Expiration_Confirmation_Email
lang: ru
---


# PGP - Extend Expiration Date - Confirmation Email
## Email to Client to confirm that their PGP key has an extended expiration date

### Body

Здравствуйте,

Спасибо за ответ. Вижу, что ваш ключ PGP обновлен и в порядке.

Мы позаботились, чтобы обновить ваш ключ и у нас на компьютерах, поэтому ваши коммуникации со Службой поддержки [И СО СПИСКАМИ, НА КОТОРЫЕ ВЫ ПОДПИСАНЫ] защищены.

Я закрываю этот запрос. Пожалуйста, свяжитесь со мной, если нужна еще какая-нибудь помощь.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles

- [Article #37: PGP - Extend Expiration Date](37-PGP_Extend_Expiration.html)
