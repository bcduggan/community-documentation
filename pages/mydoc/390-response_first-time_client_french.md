---
title: Response to 1st-Time/Non-Vetted Helpline Client_French
keywords: case handling policy, initial reply, non-vetted
last_updated: April 20, 2020
tags: [helpline_procedures_templates, templates]
summary: "First reply in French - questions and introduction to vetting"
sidebar: mydoc_sidebar
permalink: 
folder: mydoc
conf: Public
lang: fr
---


# Response to 1st-Time/Non-Vetted Helpline Client (French)
## First reply - questions and introduction to vetting

### Body

Che(è)r(e) $ClientName,

Merci d'avoir contacté Access Now Digital Security Helpline, encore une fois, je suis [IH Name]. 
Nous apprécions votre confiance dans notre service d'assistance. La ligne d'assistance en matière de sécurité numérique d'Access Now est une ressource gratuite pour la société civile du monde entier. 
Nous offrons une assistance technique directe en temps réel ainsi que des conseils aux groupes et activistes de la société civile, les médias, les journalistes, les blogueurs et les défenseurs des droits Humains.
Nous sommes heureux de vous aider, mais nous avons d'abord quelques questions à vous poser:

1. Comment avez vous entendu parler de nous? Avez-vous été référé par quelqu'un? Si c'est le cas, pouvez-vous nous dire qui?

2. Veuillez nous dire ce que vous faites. Appartenez-vous à une organisation ou un groupe?

3. Le cas échéant, pouvez-vous citer des liens vers des sites Web montrant votre travail au sein de la société civile et le mouvement des droits Humains?

4. Nous vous serions reconnaissants de bien vouloir nous indiquer le nom et le / les pronom (s) avec lesquels vous souhaiteriez qu'on utilise avec vous.


* * *


Nous tenons à vous informer que dans le cadre de nos mesures de sécurité, nous essayons de vérifier le travail de nos bénéficiaires. 
Cela signifie que nous demandons de l'aide auprès de nos partenaires de confiance en partageant avec eux des informations telles que votre nom, votre adresse e-mail et l'organisation à laquelle vous appartenez. 
Soyez assuré.e que la raison de votre prise en contact avec nous et toutes les informations relatives à votre cas seront conservées strictement confidentielles.

Pour faire avancer ce processus, il serait utile que vous puissiez nous fournir des contacts de référence (deux noms et leurs coordonnées au moins).
Si vous ne souhaitez pas que nous passions par ce processus, veuillez nous en informer dans les 48 heures.

* * *

Nous comprenons que les données que nous demandons peuvent être sensibles et confidentielles, nous devons donc établir un canal de communication sécurisé. 
Si vous utilisez déjà PGP, veuillez nous envoyer votre clé publique. Sinon, faites-nous savoir si vous utilisez un autre canal de communication sécurisé, comme Signal Messenger https://signal.org .

* * *

Veuillez noter que les services d’assistance d’Access Now ne sont pas destinés aux personnes de moins de 18 ans. Si vous, ou l'un des beneficiaires sont mineurs,
veuillez nous le signaler afin que nous puissions vous orienter vers des partenaires qui peuvent fournir l'assistance dont vous avez besoin.

Sincères amitiés,

$IH Name


### Comments

This template is editable, and must be adapted for use depending on the context of the email sent to us by the client.

In case of gendered online violence, please see [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html.html).


* * *


### Related Articles


- [Article #273: Initial Reply For French Speakers](273-Initial_Reply_For_French_Speakerss.html)
- [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html)
