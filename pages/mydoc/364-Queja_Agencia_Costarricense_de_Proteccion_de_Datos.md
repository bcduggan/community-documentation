---
title: Presentar una queja en la Agencia Costarricense de Protección de Datos
keywords: doxxing, acoso, ley de protección de datos
last_updated: October 24, 2019
tags: [harassment, articles]
summary: "La línea de ayuda recibió un caso en el que se utilizan datos personales para hostigar a un activista en Costa Rica."
sidebar: mydoc_sidebar
permalink: 364-Queja_Agencia_Costarricense_de_Protección_de_Datos.html
folder: mydoc
conf: Public
ref: Queja_Agencia_Costarricense_de_Protección_de_Datos
lang: es
---

# Presentar una queja en la Agencia Costarricense de Protección de Datos
## Instrucciones de cómo reportar el uso no consentido de datos personales en Costa Rica

### Problema

**Nota:** Este artículo aplica únicamente a casos que se reciban en el contexto Costarricense. 

Los atacantes usan datos personales para hostigar al activista en línea al exponer datos confidenciales (doxxing) o al usar imágenes del activista disponibles públicamente para promover campañas ofensivas o de difamación.

Junto con otras recomendaciones relacionadas con el acoso en línea y el intercambio no consensuado de imágenes (artículos [#234](234-FAQ-Online_Harassment.md) y [#298](298-Doxing_Non-Consensual_Publication.md)), en el contexto costarricense, estas acciones se pueden informar al [PRODHAB](http://prodhab.go.cr/).

Este reporte puede usarse como un precedente que puede legitimar la situación experimentada por los activistas, porque es una queja presentada ante una institución pública siguiendo el mecanismo disponible para este asunto, por lo que justificará un argumento (sin embargo, no puede tomarse como el argumento en sí porque no tiene el peso legal o la relevancia jurídica requerida). Tenga en cuenta (e informe a los activistas sobre esto) que el informe de la queja se puede tomar como precedente, pero no como jurisprudencia (ya que no es un caso resuelto en los tribunales).


* * *


### Solución

Lea [Article #234: FAQ - Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.md) antes de proceder con cualquier acción relacionada con este tipo de casos. Siempre se debe seguir este artículo, y se debe recomendar el procedimiento para presentar un informe a esta institución, en caso de que aplique (consulte el Artículo 2 de la Ley de Protección de Datos de Costa Rica N ° 8968). Se puede presentar denuncia contra cualquier persona mientras las hechos sean de regulación por la norma de aplicación en nuestro territorio.



#### Sobre la Ley de Protección de Datos de Costa Rica

[La ley costarricense de protección de datos N ° 8968](http://www.pgrweb.go.cr/scij/Busqueda/Normativa/Normas/nrm_texto_completo.aspx?param1=NRTC&amp;nValor1=1&amp;nValor2=70975&amp;nValor3=85989&amp;strTipM=TC) fue promulgada el 5 de septiembre de 2011. El objetivo principal de esta ley es proteger a cualquier ciudadano contra el procesamiento de sus datos personales garantizando:

- el respeto de sus derechos fundamentales y los derechos de la personalidad, específicamente, su derecho a la autodeterminación informativa.
- defensa de su libertad e igualdad respecto al tratamiento automatizado o manual de datos correspondientes a su persona o propiedad.

De acuerdo con la Ley de Protección de Datos de Costa Rica N ° 8968, el uso no consentido de datos personales (como imágenes, grabaciones de voz, etc.) se puede informar a la Agencia de Protección de Datos de los Habitantes de Costa Rica. - [PRODHAB](http://prodhab.go.cr/)). Esta Agencia debe tomar las medidas adecuadas contra el atacante, pero también, este informe puede usarse como evidencia y precedente del acoso si el activista decide construir un caso y llevarlo a los tribunales.


##### Tipo de datos

Verifique esta información antes de continuar con el reporte. Es importante verificar si la información compartida por el atacante se considera como datos personales restringidos o confidenciales de acuerdo con la ley (y, por lo tanto, la queja está justificada de acuerdo con las pautas de PRODHAB).

- **Datos personales:** cualquier información relacionada con una persona física identificable (Información de identificación personal o [PII](https://searchfinancialsecurity.techtarget.com/definition/personally-identifiable-information)).


- **Datos personales de acceso restringido:** aun siendo parte de los registros de acceso público, estos datos solo son de interés para el propietario o para la Administración pública.
    - *Ejemplos: dirección de correo electrónico, número de teléfono celular, datos del seguro, dirección física, información financiera, historial crediticio, ingresos e información laboral.*


- **Datos personales de acceso irrestricto:** los contenidos en bases de datos públicos de acceso universal, de acuerdo con leyes especiales y de acuerdo con la propuesta para la calidad de estos datos fueron recopilados.
    - *Ejemplos: nombre completo, número de identificación, edad, sexo, fecha de nacimiento, nacionalidad, firma, estado civil, número de teléfono de la casa, bienes raíces, sociedad de responsabilidad limitada - LLC (sociedades anónimas).*


- **Datos sensibles:** información relativa al fuero íntimo de la persona.
- *Ejemplos: información de salud, orientación sexual, raza / etnia, color, religión (credo), expresión de género, datos de afiliaciones sindicales (asociaciones gremiales), certificado de firma digital, PII como datos biométricos, fotografía, imágenes, voz, datos biológicos, información de control biomédico.*
    

#### Procedimiento para presentar un informe

1. Visite el sitio web oficial de la Agencia Costarricense de Protección de Datos [PRODHAB](http://prodhab.go.cr/) y seleccione la opción 'Denuncias' ubicada en la parte inferior de la página de inicio. O siga este [enlace](http://prodhab.go.cr/procedimientosdeprote/)

2. En la sección 'Procedimiento de protección de derechos' descargue el formulario 'Formulario Procedimiento de protección de derechos'. Este formulario es un documento de Word, así que guíe al activista para que ingrese todos los espacios en blanco con la información requerida. Si es necesario, comparta con el activista el documento creado por la Línea de ayuda como ejemplo (ENLACE AL DOCUMENTO).

**IMPORTANTE:** es muy importante prestar la ayuda técnica al activista con el punto *c) Aportar las pruebas documentales pertinentes (es decir, toda aquella información que consideren los denunciantes sirva de sustento a los hechos denunciados)* (es decir , toda la información que los demandantes consideran que respalda los hechos denunciados). Es posible que el activista no sea una persona técnica, por lo que es esencial ayudarlo con instrucciones detalladas sobre cómo adquirir y preservar la evidencia digital (tomando capturas de pantalla, descargando la información de la cuenta, etc.) Solicite más ayuda de otro SIH si es necesario .

3. Lea la sección 'Requisitos Adicionales'y cumpla con estos requisitos:
- Lleve un documento de identificación al momento de presentar la queja (identificación válida o pasaporte)
- La evidencia considerada pertinente para probar los hechos reportados.
- En el procedimiento se encuentran tantas copias del reclamo y su prueba, como las partes denunciadas.
- Indique la dirección física exacta de la (s) parte (s) informada (s).
- **Los documentos deben presentarse en las oficinas de PRODHAB, debidamente firmados por los demandantes.**

**NOTA:** El formulario fue preparado indicando todos los requisitos necesarios, los demandantes debe cumplir con las indicaciones para que su queja prospere, ya que si falta algo, se llevará a cabo la prevención o será rechazado.

4. Una vez que la queja se presenta en el sistema PRODHAB, los activistas deben esperar hasta la resolución del caso. Tenga en cuenta que, dado que es un procedimiento administrativo, esto implica que tiene varias fases: queja, admisibilidad, transferencia de carga, resolución final y apelación para reconsideración. El plazo dependerá de la cantidad de quejas que se estén procesando y la carga de trabajo de PROHAB.
Sin embargo, cualquier demandante puede tener acceso por vía electrónica o telefónica para consultar los casos directamente mediante el uso de la información de contacto desplegada [aquí](http://prodhab.go.cr/Contactar/). Dado que la consulta está relacionada con datos personales, la información solo se proporciona a la parte interesada en el proceso.


* * *


### Comments

#### Notes

- [Conoce las leyes que pueden ayudarte, acoso.online](https://acoso.online/pa/2-conoce-las-leyes-que-pueden-ayudarte/)
- [Protección de la Persona frente al tratamiento de sus datos personales Nº 8968](http://www.pgrweb.go.cr/scij/Busqueda/Normativa/Normas/nrm_texto_completo.aspx?param1=NRTC&amp;nValor1=1&amp;nValor2=70975&amp;nValor3=85989&amp;strTipM=TC)


* * *


### Related Articles

- [Article #234: FAQ - Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.md)
- [Article #298: Doxing and Non-Consensual Publication of Pictures and Media](298-Doxing_Non-Consensual_Publication.md)
