---
title: Initial Reply for Harassment Cases for Spanish Speakers
keywords: email templates, initial reply, case handling policy, harassment, gendered online violence
last_updated: May 29, 2020
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "First response to clients who have been targeted by gendered online violence for spanish speakers"
sidebar: mydoc_sidebar
permalink: 399-Initial_Reply_Harassment_Spanish.html
folder: mydoc
conf: Public
ref: Initial_Reply_Harassment_Spanish
lang: es
---


# Initial Reply for Harassment Cases for Spanish Speakers
## First response to clients who have been targeted by gendered online violence

### Body

Estimada/o $ClientName,

Gracias por contactar a la Línea de Ayuda de Seguridad Digital de Access Now (https://www.accessnow.org/help).

Mi nombre es $IHName, estoy aquí para asistarle, entender el problema que está experimentando en este momento y darle las mejores recomendaciones de qué hacer a continuación.  Hemos recibido su solicitud inicial y ya nos encontramos trabjando en ello.  Para asistirle y entender mejor sus necesidades, necesitamos realizarle algunas preguntas y darle seguimiento a lo que le está sucediendo.  Por favor tómese su tiempo en responder a estas preguntas: 

* ¿Sería apropiado si realizamos una llamada ya sea videollamada o solo una llamada de audio?
* Si usted prefiere que alguien hable por usted, por favor hágamelo saber.  Nos encontramos  listos para conversar con alguna persona en la que usted confíe que pueda proveer detalles de su caso y poder intercabiar correos.
* Si prefiere conversar con alguien que se identifique como mujer, por favor hágamelo saber y le transfiero el caso a alguna de mis colegas.

Antes de que volvamos a conversar, le quiero solicitar que nos ayude manteninedo registro de lo que sucede, guardando información importante y buscando una persona cercana de confianza que le pueda confortar y ayudarle dándole una mano para manejar este incidente que está experimentando.

Por favor conteste este mensaje incluyendo el tag “[accessnow #ID]” en el asunto y sus respuestas a las preguntas antes indicadas.

Saludos cordiales y mantente fuerte.

$IHName



* * *


### Related Articles

[Article #234: Online Harassment Targeting a Civil Society Member](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Harassment/234-FAQ-Online_Harassment.md)
[Article #268: Initial Reply for Harassment Cases](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Templates/268-Initial_Reply_Harassment.md#initial-reply-for-harassment-cases)
